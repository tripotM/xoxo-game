﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XOXO_Game.Classes;

namespace XOXO_Game.Classes
{
    class Game
    {
        
        public Game() { }

        public int gameask()
        {
            Console.WriteLine("Bonjour voulez jouer en \n [1] : Solo \n [2] : Multi ");
            string rep = Console.ReadLine();
            return Convert.ToInt32(rep);
        }
        public void gameStart()
        {
            int response = 0;
            bool verif = false; 
            
            while (!verif)
            {
                response = gameask();
                if (response == 1)
                {
                    verif = true;
                }
                else if (response == 2)
                {
                    verif = true;
                }
            }
            
            if (response == 1)
            {
                SoloGame();
            }
            else if(response == 2)
            {
                Multigame();
            }


        }
        #region Solo
        // Solo Game 
        public void SoloGame()
        {
            int X;
            int Y;


            
            Plateau plateau = new Plateau();
            // creer les joueur 
            Console.WriteLine("Nom du joueur 1 ");
            string nom = Console.ReadLine();
            Player player1 = new Player(" X", nom);

            Console.WriteLine("Nom du Méchant ");
            nom = Console.ReadLine();
            Enemy enemy = new Enemy(new Player(" O", nom));




            plateau.contruct();
            // Affichage du nombre de points 

            plateau.Affichage();


            while (plateau.IsComplete() == false)
            {
                // joueur 1 
                Console.WriteLine(" c'est à " + player1.Name + " De jouer ");


                // placer le pion X                       
                X = plateau.Numberverif("Ligne");
                // placer le pion O 
                Y = plateau.Numberverif("Colonne");

                Console.WriteLine("\n vous avez placer votre pion en " + X + " " + Y);
                //Ajoute le pion sur le plateau 
                plateau.Add(X, Y, player1.Pion);

                Console.ReadKey();
                Console.Clear();
                plateau.Affichage();

                if (plateau.IsComplete() == true)
                {
                    break;
                }
                // joueur 2 
                Console.WriteLine(" c'est à " + enemy.name + " De jouer ");


                // placer le pion X                       
                X = plateau.Numberverif("Ligne");
                // placer le pion O 
                Y = plateau.Numberverif("Colonne");

                Console.WriteLine("\n vous avez placer votre pion en " + X + " " + Y);
                //Ajoute le pion sur le plateau 
                plateau.Add_O(enemy.placement(), enemy.placement());

                Console.ReadKey();
                Console.Clear();
                plateau.Affichage();
                if (plateau.IsComplete() == true)
                {
                    break;
                }

                plateau.NombreTour++;
                plateau.Compte();
            }

            Console.WriteLine("\n Partie Terminée");
            plateau.Gagnant(player1, enemy.player);

            Console.ReadKey();

        }
        #endregion
        #region Multi 
        // 2 player game 
        public void Multigame()
        {
            int NombrePoint = 0;
            int X;
            int Y;
            

            Enemy enemy = new Enemy();
            Plateau plateau = new Plateau();
            // creer les joueur 
            Console.WriteLine("Nom du joueur 1 ");
            string nom = Console.ReadLine();
            Player player1 = new Player(" X", nom);
            
            Console.WriteLine("Nom du joueur 2 ");
            nom = Console.ReadLine();
            Player player2 = new Player(" O", nom);




            plateau.contruct();
            // Affichage du nombre de points 

            plateau.Affichage();


            while (plateau.IsComplete() == false)
            {
                // joueur 1 
                Console.WriteLine(" c'est à " + player1.Name + " De jouer ");


                // placer le pion X                       
                X = plateau.Numberverif("Ligne");
                // placer le pion O 
                Y = plateau.Numberverif("Colonne");

                Console.WriteLine("\n vous avez placer votre pion en " + X + " " + Y);
                //Ajoute le pion sur le plateau 
                plateau.Add(X, Y,player1.Pion);

                Console.ReadKey();
                Console.Clear();
                plateau.Affichage();

                if (plateau.IsComplete() == true)
                {
                    break;
                }
                // joueur 2 
                Console.WriteLine(" c'est à " + player2.Name + " De jouer ");


                // placer le pion X                       
                X = plateau.Numberverif("Ligne");
                // placer le pion O 
                Y = plateau.Numberverif("Colonne");

                Console.WriteLine("\n vous avez placer votre pion en " + X + " " + Y);
                //Ajoute le pion sur le plateau 
                plateau.Add(X, Y,player2.Pion);

                Console.ReadKey();
                Console.Clear();
                plateau.Affichage();
                if (plateau.IsComplete() == true)
                {
                    break;
                }

                plateau.NombreTour++;
                plateau.Compte();
            }

            Console.WriteLine("\n Partie Terminée");
            plateau.Gagnant(player1, player2);

            Console.ReadKey();
        }
        #endregion
    }
}
