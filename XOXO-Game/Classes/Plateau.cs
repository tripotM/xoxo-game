﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XOXO_Game.Classes
{
    class Plateau
    {
        public Game game = new Game();
        public Enemy enemy = new Enemy();
        public int NombreX;
        public int NombreO;
        public int NombreTour;
        private string Def;
        private string X;
        private string O; 

        public string[][] lePlateau;


        // creation du plateau 
        public Plateau()
        {
            this.Def = " .";
            this.X = " X";
            this.O = " O";
            this.NombreO = 0;
            this.NombreX = 0;
            this.NombreTour = 1;
            this.lePlateau = new string[11][];
        }

        public void contruct()
        {
            //fabrication du plateau 
            for (int i = 1; i < 11; i++)
            {
                lePlateau[i] = new string[11];

                for (int a = 0; a < 11; a++)
                {
                    lePlateau[i][a] = " .";
                }
            }


            //faire la ligne avec les numéros de colonnes
            lePlateau[0] = new string[11];
            for (int i = 0; i < 11; i++)
            {
                lePlateau[0][i] = Convert.ToString(" " + i);
            }

            //Numérotation des lignes 
            for (int i = 0; i < 11; i++)
            {
                if (i == 10)
                    lePlateau[i][0] = Convert.ToString(i);
                else
                    lePlateau[i][0] = Convert.ToString(" " + i);
            }
        }

        public void Affichage()
        {
            Console.WriteLine("Nombre de X: " + NombreX);
            Console.WriteLine("Nombre de O: " + NombreO);
            Console.WriteLine("--- Tour " + NombreTour + " ---");
            foreach (var p in lePlateau)
            {
                Array.ForEach(p, Console.Write);
                Console.WriteLine();
            }
        }

        public void Add_X(int x, int y)
        {
            // le X
            if (lePlateau[x][y] != Def)
            {
                Console.WriteLine("Il y a déjà un pion sur la position choisie veuiller choisir un autre emplacement");
                Numberverif("Ligne");
                Numberverif("Colonne");
            }
            lePlateau[x][y] = X;
            //Todo verifier qu'il ny ai pas deja un piont

            // le X deux case en dessous 
            // verifie que ça soit dans le plateau
            if(y+2<11 && lePlateau[x][y+2] == Def)
                lePlateau[x][y+2] = X;
        }
        
        public void Add_O(int x, int y)
        {
            // le O
            //Todo verifier qu'il ny ai pas deja un piont
            while(lePlateau[x][y] != Def)
            {
                x = enemy.placement();
                y = enemy.placement();
            }
            lePlateau[x][y] = O;
            Console.WriteLine("le pion O à été placer en " + x + " "+ y);
            // le O deux case a droite 
            // verifie que ça soit dans le plateau
            if (x + 2 < 11 && lePlateau[x+2][y] == Def)
                lePlateau[x+2][y] = O;
            
        }
        public int Numberverif(string L)
        {
            int res;
            do
            {
                Console.WriteLine("La " + L + " que vous avez rentrez n'est pas valide");
                res = AskNumber(L);
            }
            while (res < 1 || res > 10);


            return res;
        }
        //demande le pion 
        public int AskNumber(string L)
        {
            int res;
            bool isint;
            Console.WriteLine("\n sur quelle " + L + " souhaitez vous placer votre pion ?");
            //int isint = Convert.ToInt32(Console.ReadLine());
            isint = int.TryParse(Console.ReadLine(),out res);
            return res;
        }

        // verifie si le plateau est complet 
        public bool IsComplete()
        {
            foreach (var Ligne in lePlateau)
            {
                foreach (var P in Ligne)
                {
                    if (P == Def)
                    {
                        return false;
                    }
                }
            }
            return true; 
        }

        // ajout pour le multi le temps de fair un truc bien 
        public void Add(int x, int y, string lepion)
        {
            // le X
            if (lePlateau[x][y] != Def)
            {
                Console.WriteLine("Il y a déjà un pion sur la position choisie veuiller choisir un autre emplacement");
                Numberverif("Ligne");
                Numberverif("Colonne");
            }
            lePlateau[x][y] = lepion;

            // le X deux case en dessous 
            // verifie que ça soit dans le plateau
            if (lepion == X )
            {
                if (y + 2 < 11 && lePlateau[x][y + 2] == Def)
                    lePlateau[x][y + 2] = lepion;
            }
            else if (lepion == O)
            {
                if (x + 2 < 11 && lePlateau[x+2][y] == Def)
                    lePlateau[x + 2][y] = lepion;
            }


        }
        public void Compte()
        {

            // boucle foreach mes couilles 
            // if X +1 
            //if O +1 
            NombreO = 0;
            NombreX = 0;
            foreach (var Ligne in lePlateau)
            {
                foreach (var P in Ligne)
                {
                    if (P == X)
                    {
                        NombreX++;
                    }

                    if (P == O)
                    {
                        NombreO++;
                    }
                }
            }

        }
        public void Gagnant(Player P1, Player P2)
        {
            
            if (NombreX > NombreO)
            {
                Console.WriteLine(P1.Name + " à ganger avec un total de " + NombreX + "points");
            }
            else
            {
                Console.WriteLine(P2.Name + " à ganger avec un total de " + NombreO + "points");
            }
            




        }
    }
}
